function sortNumber(a,b) {
    return a - b;
}

 module.exports =  function (total, moneyPaid, coins) {
 	coins = coins.sort(sortNumber);
 	var n = moneyPaid - total;
 	var res = [];
 	if (n < 0) {
 		throw new Error("paid money is less than total money")
 	} else if (n == 0) {
 		return []
 	} else {
 		for (let i = coins.length - 1; i >= 0 ;i--) {
			if (coins[i] <= n) {
				res.push(coins[i]);
				n -= coins[i];
				if (coins[i] <= n) {
					i += 1;
				}
			}
 		}
 		if (res.length !== 0) {
 			return res;
 		} else {
 			throw new Error("there is no way can make a change")

 		}
 	}
 	
 	
 	
}